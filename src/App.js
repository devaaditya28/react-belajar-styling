import 'bootstrap/dist/css/bootstrap.min.css';
import NavbarMenu from './component/Navbar';
import './App.css';
function App() {
  return (
    <div className="App">
      <NavbarMenu />
    </div>
  );
}

export default App;
